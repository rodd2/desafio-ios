//
//  ViewController.m
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//
#import "ViewController.h"
#import "WebService.h"
#import "CEP.h"
#import "Mantle.h"
#import "DataHandler.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //limita o numero de caracters
    if (range.location == (9)) {
        return NO;
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
    [formatter setGroupingSeparator:@"-"];
    [formatter setGroupingSize:2];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setSecondaryGroupingSize:6];
    NSString *cep = textField.text ;
    if(![cep isEqualToString:@""])
    {
        cep= [cep stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[cep doubleValue]]];
        textField.text=str;
    }
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_txtCEP resignFirstResponder];
}

- (IBAction)consultar:(id)sender {
    [_txtCEP resignFirstResponder];
    if(_txtCEP.text.length == 9){
        
        WebService *ws = [WebService sharedWebService];
        [_spinner startAnimating];
        
        _textArea.text = @"Buscando CEP...";
        
        [ws getCEP:[self removeMask:_txtCEP.text] success:^(NSURLSessionDataTask *task, id responseObject){
            CEP *cep = [MTLJSONAdapter modelOfClass:CEP.class fromJSONDictionary:responseObject error:nil];
            [[DataHandler sharedHandler] save:cep];
            [self showCEP:cep];
            [_spinner stopAnimating];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            if (error.code == NSURLErrorNotConnectedToInternet) {
                _textArea.text = @"Você precisa estar conectado(a) à internet.";
            } else {
                //error 404
                if (error.code == NSURLErrorBadServerResponse) {
                    _textArea.text = @"Cep não encontrado.";
                } else {
                    if (error.code == NSURLErrorTimedOut) {
                        _textArea.text = @"O tempo de conexão se esgotou.";
                    } else {
                        _textArea.text = [NSString stringWithFormat:@"%@",error.description];
                    }
                }
            }
            [_spinner stopAnimating];
        }];
    }else{
        _textArea.text = @"CEP inválido. Por favor, preencha o CEP de acordo: Ex 66000-000.";
        [_spinner stopAnimating];
    }
}


-(NSString *)removeMask:(NSString *)cep
{
    return [NSString stringWithFormat:@"%@%@",[cep substringToIndex:5],[cep substringFromIndex:6]];
}

-(void)showCEP:(CEP *)cep
{
    NSString *result = [NSString stringWithFormat:@"ENDEREÇO ENCONTRADO:\n\nCEP: %@\nBAIRRO: %@\n%@: %@\nCIDADE: %@\nESTADO: %@",cep.cep,cep.bairro,cep.tipoLogradouro,cep.logradouro,cep.cidade,cep.estado];
    _textArea.text = result;
}
@end
