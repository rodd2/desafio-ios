//
//  HistoryTableViewCell.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (nonatomic, assign) IBOutlet UILabel *cep;
@property (nonatomic, assign) IBOutlet UILabel *cidadeEstado;
@property (nonatomic, assign) IBOutlet UILabel *tipoELougradoro;
@property (nonatomic, assign) IBOutlet UILabel *data;
@end
