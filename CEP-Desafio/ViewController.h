//
//  ViewController.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UITextView *textArea;
@property (weak, nonatomic) IBOutlet UITextField *txtCEP;
- (IBAction)consultar:(id)sender;

@end
