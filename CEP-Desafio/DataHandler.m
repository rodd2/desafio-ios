//
//  DataHandler.m
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/11/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import "DataHandler.h"

@implementation DataHandler

+(DataHandler *) sharedHandler
{
    static DataHandler *_dataHandler = nil;
    @synchronized([DataHandler class]) {
        if (!_dataHandler)
        {
            _dataHandler = [[self alloc] init];
        }
        
        return _dataHandler;
    }
    return nil;
}

-(BOOL)save:(CEP *)cep;
{
    NSDictionary *dictionary = [MTLJSONAdapter JSONDictionaryFromModel:cep];
    NSString *path = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"%@/%@",CEP_DIR,cep.cep]];
    
    return [dictionary writeToFile:path atomically:YES];
}

-(NSArray *)load
{
    NSError *error = nil;
    NSArray *cep = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSHomeDirectory() stringByAppendingString:CEP_DIR] error:&error];
    
    if (cep)
        return cep;
    else
        return nil;
}

-(void)createDiretory
{
    NSError *error = nil;
    NSString *path = [NSHomeDirectory() stringByAppendingString:CEP_DIR];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

@end
