//
//  WebService.m
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import "WebService.h"

@implementation WebService

+(WebService *) sharedWebService
{
    static WebService *_webservice = nil;
    @synchronized([WebService class]) {
        if (!_webservice)
        {
            _webservice = [[self alloc] init];
        }
        
        return _webservice;
    }
    return nil;
}

-(void)getCEP:(NSString *)cep success:(void(^)(NSURLSessionDataTask *task, id responseObject))success failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure
{
    NSString *path = [URL stringByAppendingString:[NSString stringWithFormat:@"/%@",cep]];
    
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (success) {
            success(task, responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure) {
            failure(task, error);
        }
    }];
}

@end
