//
//  CEP.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import "Mantle.h"
#import "Constante.h"

@interface CEP : MTLModel <MTLJSONSerializing>

@property (readonly, nonatomic, copy) NSString *bairro;
@property (readonly, nonatomic, copy) NSString *cep;
@property (readonly, nonatomic, copy) NSString *cidade;
@property (readonly, nonatomic, copy) NSString *estado;
@property (readonly, nonatomic, copy) NSString *logradouro;
@property (readonly, nonatomic, copy) NSString *tipoLogradouro;
@property (readonly, nonatomic, copy) NSString *searchDate;
@end
