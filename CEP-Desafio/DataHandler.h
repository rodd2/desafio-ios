//
//  DataHandler.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/11/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEP.h"
#import "Mantle.h"
#import "Constante.h"

@interface DataHandler : NSObject

+(DataHandler *) sharedHandler;
-(BOOL)save:(CEP *)cep;
-(NSArray *)load;
-(void)createDiretory;
@end
