//
//  HistoryTableViewController.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//
#import "Constante.h"
#import <UIKit/UIKit.h>

@interface HistoryTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *cepHistory;
}
@end
