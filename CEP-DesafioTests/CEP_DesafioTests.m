//
//  CEP_DesafioTests.m
//  CEP-DesafioTests
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#define EXP_SHORTHAND

#import <OCMock/OCMock.h>
#import "Specta.h"
#import "Expecta.h"
#import "ViewController.h"
#import "HistoryTableViewController.h"
#import "WebService.h"

SpecBegin(ViewController)

describe(@"ViewController", ^{
    __block ViewController *vc;
    
    beforeEach(^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [vc loadView];
        UIView *view = vc.view;
        expect(view).toNot.beNil;
    });
    
    it(@"ViewController should not be nil", ^{
        expect(vc).toNot.beNil;
        expect(vc).to.beInstanceOf([ViewController class]);
    });
    
    it(@"Ter campo de CEP do tipo UITextField", ^{
        expect(vc.txtCEP).toNot.beNil;
        expect(vc.txtCEP).to.beInstanceOf([UITextField class]);
        expect(vc.txtCEP.delegate).notTo.beNil;
        expect(vc.txtCEP.delegate).to.equal(vc);
    });
    
    it(@"Ter area de texto do tipo UITextView", ^{
        expect(vc.textArea).toNot.beNil;
        expect(vc.textArea).to.beInstanceOf([UITextView class]);
    });
    
    it(@"Ter spinner do tipo UIActivityIndicatorView", ^{
        expect(vc.spinner).toNot.beNil;
        expect(vc.spinner).to.beInstanceOf([UIActivityIndicatorView class]);
    });
    
    it(@"request with empty CEP", ^{
        vc.txtCEP.text = @"";
        [vc consultar:nil];
        
        expect(vc.textArea.text).to.equal(@"CEP inválido. Por favor, preencha o CEP de acordo: Ex 66000-000.");
    });
    
    it(@"request with not full CEP", ^{
        vc.txtCEP.text = @"66044";
        [vc consultar:nil];
        
        expect(vc.textArea.text).to.equal(@"CEP inválido. Por favor, preencha o CEP de acordo: Ex 66000-000.");
    });
});

describe(@"WebService", ^{
    __block id _mockWS;
    
    beforeEach(^{
        _mockWS = [OCMockObject partialMockForObject:[WebService sharedWebService]];
    });
    
    afterEach(^{
        [_mockWS verify];
    });
    
    it(@"search for a null CEP", ^{
        [_mockWS getCEP:@"" success:^(NSURLSessionDataTask *task, id responseObject) {
            expect(responseObject).toNot.beNil;
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            expect(error).toNot.beNil;
        }];
    });
    
    it(@"search for a not full CEP", ^{
        [_mockWS getCEP:@"66055" success:^(NSURLSessionDataTask *task, id responseObject) {
            expect(responseObject).toNot.beNil;
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            expect(error).toNot.beNil;
        }];
    });
    
    it(@"search for a full CEP", ^{
        [_mockWS getCEP:@"66055280" success:^(NSURLSessionDataTask *task, id responseObject) {
            expect(responseObject).toNot.beNil;
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            expect(error).toNot.beNil;
        }];
    });
    
});

describe(@"HistoryViewController", ^{
    __block HistoryTableViewController *hvc;
    
    beforeEach(^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        hvc = [storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
        [hvc loadView];
        UIView *view = hvc.view;
        expect(view).toNot.beNil;
    });
    
    it(@"HistoryViewController should not be nil", ^{
        expect(hvc).toNot.beNil;
        expect(hvc).to.beInstanceOf([HistoryTableViewController class]);
    });
    
    it(@"HistoryViewController table should have delegate and datasource", ^{
        expect(hvc.tableView.delegate).toNot.beNil;
        expect(hvc.tableView.delegate).to.equal(hvc);
        expect(hvc.tableView.dataSource).toNot.beNil;
        expect(hvc.tableView.dataSource).to.equal(hvc);
    });
});

SpecEnd
