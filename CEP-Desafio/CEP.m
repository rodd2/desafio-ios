//
//  CEP.m
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//

#import "CEP.h"

@implementation CEP

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{
             @"bairro": @"bairro",
             @"cep": @"cep",
             @"cidade" : @"cidade",
             @"estado" : @"estado",
             @"logradouro" : @"logradouro",
             @"tipoLogradouro": @"tipoDeLogradouro",
    };
}

+ (NSValueTransformer *)cepJSONTransformer {
    return [MTLValueTransformer transformerWithBlock:^(NSString *str) {
        if(str.length == 8)
            return [NSString stringWithFormat:@"%@-%@",[str substringToIndex:5],[str substringFromIndex:5]];
        else
            return str;
    }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDictionary *defaults = @{@"searchDate" : [formatter stringFromDate:[NSDate date]]};
    dictionaryValue = [defaults mtl_dictionaryByAddingEntriesFromDictionary:dictionaryValue];
    self = [super initWithDictionary:dictionaryValue error:error];
    
    if(self == nil) return nil;
    
    return self;
    
}

@end
