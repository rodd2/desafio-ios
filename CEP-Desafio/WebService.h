//
//  WebService.h
//  CEP-Desafio
//
//  Created by Rodrigo Cavalcante on 8/9/14.
//  Copyright (c) 2014 Rodrigo Cavalcante. All rights reserved.
//
#import "Constante.h"
#import <AFNetworking/AFNetworking.h>
#import <Foundation/Foundation.h>

@interface WebService : AFHTTPSessionManager

+(WebService *) sharedWebService;
-(void)getCEP:(NSString *)cep success:(void(^)(NSURLSessionDataTask *task, id responseObject))success failure:(void(^)(NSURLSessionDataTask *task, NSError *error))failure;
@end
